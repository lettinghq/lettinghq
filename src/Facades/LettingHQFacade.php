<?php
namespace LettingHQ\LettingHQ\Facades;

use Illuminate\Support\Facades\Facade as IlluminateFacade;

class LettingHQFacade extends IlluminateFacade {
    protected static function getFacadeAccessor() {
        return 'lettinghq';
    }
}
