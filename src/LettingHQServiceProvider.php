<?php
namespace LettingHQ\LettingHQ;

use Illuminate\Support\ServiceProvider;

class LettingHQServiceProvider extends ServiceProvider {
    public function register() {
        $this->app->singleton('lettinghq', function($app) {
            return new LettingHQ();
        });
    }

    public function boot() {
        $this->publishes([
            __DIR__ . '/config/lettinghq.php' => config_path('lettinghq.php'),
        ]);
    }

    public function provides() {
        return [
            'lettinghq',
        ];
    }
}
